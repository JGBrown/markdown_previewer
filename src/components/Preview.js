import React from 'react';
import marked  from 'marked';
import './Preview.css';
import Markdown from 'markdown-to-jsx';


class Preview extends React.Component {
  getMarkdownText() {
    marked.setOptions({
breaks: true
})
     var rawMarkup = marked(this.props.text, {sanitize: true});
     return { __html: rawMarkup };
   }


  render(){
return (
    <div className="previewSec">
      <div id="previewHeader"> HTML Markdown</div>
        <div id="preview" dangerouslySetInnerHTML={this.getMarkdownText()} >
          </div>
  </div>
  );
}
}




export default Preview;
