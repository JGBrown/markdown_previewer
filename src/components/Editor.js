import React from 'react';
import Preview from "./Preview";
import marked  from 'marked';
import sanitizeHtml from 'sanitize-html';
import Markdown from 'markdown-to-jsx';

  const placeholder = `# This is a Header
  ## header2
  ### header3
  [link]("www.google.com")
  * Unordered list can use asterisks
  1. First ordered list item
  ![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
    You can also make text **bold**... whoa!
    Heres some code, \`<div></div>\`, between 2 backticks.
    \`\`\`javascript
    var s = "JavaScript syntax highlighting";
    alert(s);
    \`\`\`
    \> We're living the future so
  `

class Editor extends React.Component {

  constructor(props) {
         super(props);
         this.state = {
           text: placeholder
          };
     }

  updateText = (event) => {
     console.log("EVent:", event.target.value)
      event.preventDefault();
      this.setState({text: event.target.value});
  }

  render() {
        return (
          <div >
            <div> Text to Markdown</div>
            <textarea
              id="editor"
              onChange={this.updateText}
              value={this.state.text}></textarea>
            <Preview text = {this.state.text} />
          </div>
        )

}};

export default Editor;
